elec_2008 = "http://rsl00.epimg.net/elecciones/2008/generales/congreso"
elec_2011 = "http://rsl00.epimg.net/elecciones/2011/generales/congreso"

def solicite (url)
  xml_data = Net::HTTP.get_response(URI.parse(url))
  @doc = Hpricot::XML(xml_data.body)
end

def compare (url)
  xml_data = Net::HTTP.get_response(URI.parse(url))
  @compara = Hpricot::XML(xml_data.body)
end

def ganadores (url)
  xml_data = Net::HTTP.get_response(URI.parse(url))
  @ganadores = Hpricot::XML(xml_data.body)
end

  #url = "http://rsl00.epimg.net/elecciones/2008/generales/congreso/01/index.xml2"
  # xml_data = Net::HTTP.get_response(URI.parse(url)).body
  #
  #   @doc = Hpricot::XML(xml_data)

def ts(st)
    st.gsub(/(\d)(?=(\d\d\d)+(?!\d))/, "\\1.")
end

def comunidad(name)
  @comunidades[name.to_sym]
end

get "/generales/" do
  solicite("#{elec_2011}/index.xml2")
  compare("#{elec_2008}/index.xml2")
  ganadores("http://rsl00.epimg.net/elecciones/2011/generales/congreso/partidosGanadores/pgCIR.xml2")

  def color (id)
    codigo_color = (@ganadores / "//upo/id[text()='#{id}']/../codigo_color" ).text
    if codigo_color == "d"
      return "{fillColor:'025D8C', strokeColor:'013c5a', strokeOpacity: 1, fillOpacity: 1}"
    elsif codigo_color == "i"
      return "{fillColor:'A80000', strokeColor:'860000', strokeOpacity: 1, fillOpacity: 1}"
    elsif codigo_color == "o"
      return "{fillColor: 'FF9500', strokeColor:'cc7700', strokeOpacity: 1, fillOpacity: 1}"
    end
  end
  @title = "Generales 2011"
  erb :"/generales/index"
end

get "/generales/:comunidades" do
  @comunidad = params[:comunidades]
  @provincia = params[:provincias]
  solicite("#{elec_2011}/#{comunidad(@comunidad)[0]}/index.xml2")
  compare("#{elec_2008}/#{comunidad(@comunidad)[0]}/index.xml2")
  @title = "Generales en " + comunidad(@comunidad)[1] + " 2011"
  erb :"/generales/comunidades"
end

get "/generales/:comunidades/:provincias" do
   @comunidad = params[:comunidades]
   @provincia = params[:provincias]
   solicite("#{elec_2011}/#{comunidad(@comunidad)[0]}/#{comunidad(@comunidad)[2][@provincia.to_sym][0]}.xml2")
   compare("#{elec_2008}/#{comunidad(@comunidad)[0]}/#{comunidad(@comunidad)[2][@provincia.to_sym][0]}.xml2")
   @title = "Generales en " + comunidad(@comunidad)[2][@provincia.to_sym][1] + " 2011"
  erb :"generales/provincias"
end

get "/generales/:comunidades/:provincias/:municipios" do
   @comunidad = params[:comunidades]
   @provincia = params[:provincias]
   @municipio = params[:municipios]
  solicite("#{elec_2011}/#{comunidad(@comunidad)[0]}/#{comunidad(@comunidad)[2][@provincia.to_sym][0]}/#{comunidad(@comunidad)[2][@provincia.to_sym][2][@municipio.to_sym][0]}.xml2")
  @title = "Generales en " + comunidad(@comunidad)[2][@provincia.to_sym][2][@municipio.to_sym][1] + " 2011"
  erb :"generales/municipios"
end