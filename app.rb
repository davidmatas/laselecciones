# encoding: utf-8

require 'date'
require 'to_slug'
require 'net/http'
require 'hpricot'
require 'sinatra'
require_relative 'data.rb'

load 'generales.rb'

elec_2008_an = "http://rsl00.epimg.net/elecciones/2008/autonomicas/01"
elec_2012_an = "http://rsl00.epimg.net/elecciones/2012/autonomicas/01"

elec_2007_as = "http://rsl00.epimg.net/elecciones/2007/autonomicas/03"
elec_2012_as = "http://rsl00.epimg.net/elecciones/2012/autonomicas/03"

def solicite (url)
  xml_data = Net::HTTP.get_response(URI.parse(url))
  @doc = Hpricot::XML(xml_data.body)
end

def compare (url)
  xml_data = Net::HTTP.get_response(URI.parse(url))
  @compara = Hpricot::XML(xml_data.body)
end

def ganadores (url)
  xml_data = Net::HTTP.get_response(URI.parse(url))
  @ganadores = Hpricot::XML(xml_data.body)
end

  #url = "http://rsl00.epimg.net/elecciones/2008/generales/congreso/01/index.xml2"
  # xml_data = Net::HTTP.get_response(URI.parse(url)).body
  #
  #   @doc = Hpricot::XML(xml_data)

def ts(st)
    st.gsub(/(\d)(?=(\d\d\d)+(?!\d))/, "\\1.")
end

def comunidad(name)
  @comunidades[name.to_sym]
end

get "/" do
  @doc_an = solicite("#{elec_2012_an}/index.xml2")
  @compara_an = compare("#{elec_2008_an}/index.xml2")

  @doc_as = solicite("#{elec_2012_as}/index.xml2")
  @compara_as = compare("#{elec_2007_as}/index.xml2")

  erb :index
end

get "/:comunidades" do
  @comunidad = params[:comunidades]
  @provincia = params[:provincias]
  if @comunidad == "andalucia"
    solicite("#{elec_2012_an}/index.xml2")
    compare("#{elec_2008_an}/index.xml2")

    elsif @comunidad == "asturias"
    solicite("#{elec_2012_as}/index.xml2")
    compare("#{elec_2007_as}/index.xml2")

   end
  @title = "Autonómicas en " + comunidad(@comunidad)[1]
  erb :comunidades
end

get "/:comunidades/:provincias" do
   @comunidad = params[:comunidades]
   @provincia = params[:provincias]
   if @comunidad == "andalucia"
    solicite("#{elec_2012_an}/#{comunidad(@comunidad)[2][@provincia.to_sym][0]}.xml2")
    compare("#{elec_2008_an}/#{comunidad(@comunidad)[2][@provincia.to_sym][0]}.xml2")

    elsif @comunidad == "asturias"
    solicite("#{elec_2012_as}/#{comunidad(@comunidad)[2][@provincia.to_sym][0]}.xml2")
    compare("#{elec_2007_as}/#{comunidad(@comunidad)[2][@provincia.to_sym][0]}.xml2")

   end
   @title = "Autonómicas en " + comunidad(@comunidad)[2][@provincia.to_sym][1]
  erb :provincias
end

get "/:comunidades/:provincias/:municipios" do
   @comunidad = params[:comunidades]
   @provincia = params[:provincias]
   @municipio = params[:municipios]
   if @comunidad == "andalucia"
    solicite("#{elec_2012_an}/#{comunidad(@comunidad)[2][@provincia.to_sym][0]}/#{comunidad(@comunidad)[2][@provincia.to_sym][2][@municipio.to_sym][0]}.xml2")

    elsif @comunidad == "asturias"
    solicite("#{elec_2012_as}/#{comunidad(@comunidad)[2][@provincia.to_sym][0]}/#{comunidad(@comunidad)[2][@provincia.to_sym][2][@municipio.to_sym][0]}.xml2")

   end

  @title = "Autonómicas en " + comunidad(@comunidad)[2][@provincia.to_sym][2][@municipio.to_sym][1]
  erb :municipios
end





